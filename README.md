# HAN IoT Portfolio

This repository contains the actual portfolio assignments and examples for HAN HM ESE IoT classes that is delivered to students that follow eduaction at HAN Engineering in parttime.

## Use of submodules
This repository is using nested git repositories as submodules. Submodules are repositories that are included in the base repository and are complementary. Submodule repositories are maintained elsewhere and shall be pulled separately after cloning this repository. 

To clone this repository including its submodules you can use the ```--recursive``` parameter.

```
git clone --recursive [URL to Git repo]
```

If you already have cloned the repository and now want to load it’s submodules you have to use submodule update.

```
git submodule update --init
# if there are nested submodules:
git submodule update --init --recursive
```

# Setup your portfolio
Setting up your portfolio can be done following these steps: 

## 1. Create a repository using this template
To [Import project from repository by URL](https://docs.gitlab.com/ee/user/project/import/repo_by_url.html) you can import this repositories by providing the Git URL:

 1. On the top bar, select Menu > Create new project.
 2. Select the Import project tab.
 3. Select Repository by URL.
 4. Enter the Git repository URL (https://gitlab.com/wlgrw/han-iot-portfolio.git).
 5. Complete the remaining fields.
 6. Select Create project.

Your newly created project is displayed

## 2. Set your repository "Private"
*This is a mandatory requirement.*

**Prerequisite: You must have the Owner role for a project.**
To [Change project visibility](https://docs.gitlab.com/ee/user/public_access.html#change-project-visibility):

 1. On the top bar, select Menu > Projects and find your project.
 2. On the left sidebar, select Settings > General.
 3. Expand Visibility, project features, permissions.
 4. Change Project visibility to either **Private**.
 5. Select Save changes.

## 3. Allow assessor as Reporter
*This is a mandatory requirement.*

Add your assessor as **Reporter** to your portfolio repository. 

# Disclaimer
The content of this repository is provided in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  
# License
This repository is part of the IoT classes delivered at HAN Engineering Embedded Systems Engineering (ESE). This repository is free: You may redistribute it and/or modify it under the terms of a Creative  Commons Attribution-NonCommercial 4.0 International License  (http://creativecommons.org/licenses/by-nc/4.0/) by Remko Welling (https://ese.han.nl/~rwelling/) E-mail: remko.welling@han.nl 

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Attribution-NonCommercial 4.0 International License</a>.