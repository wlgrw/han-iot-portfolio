# HAN IoT Portfolio - Payload-encoder -decoder

## Instruction
This folder holds the code and documentation for the Payload-encoder -decoder assignment. 

The folder shall hold two sub-folders with the following names:
 - myPayloadEncoderDecoder
 - myPayloadEncoderDecoder-docs
 
### Folder myPayloadEncoderDecoder
This folder holds the source code of the Payload-encoder -decoder assignment. 

### Folder myPayloadEncoderDecoder-docs
This folder contains the mandatory files required to generate documentation from the annotation in de source code of the Payload-encoder -decoder assignment. 

Apart from a subfolder where images are stored, a ```.doxy```-file containing additional documentation, and a doxy file with the configuration is stored. 
 

# Disclaimer
The content of this repository is provided in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  
# License
This repository is part of the IoT classes delivered at HAN Engineering Embedded Systems Engineering (ESE). This repository is free: You may redistribute it and/or modify it under the terms of a Creative  Commons Attribution-NonCommercial 4.0 International License  (http://creativecommons.org/licenses/by-nc/4.0/) by Remko Welling (https://ese.han.nl/~rwelling/) E-mail: remko.welling@han.nl 

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Attribution-NonCommercial 4.0 International License</a>.