# HAN IoT Portfolio - Node-RED flow

This folder shall hold the export of your Node-RED flow.
Documentation on importen and exporting flos in Node-RED can be found [here](https://nodered.org/docs/user-guide/editor/workspace/import-export).

Save your flow as a Javascript ```.js```-file and add your file to the repository. 

# Disclaimer
The content of this repository is provided in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  
# License
This repository is part of the IoT classes delivered at HAN Engineering Embedded Systems Engineering (ESE). This repository is free: You may redistribute it and/or modify it under the terms of a Creative  Commons Attribution-NonCommercial 4.0 International License  (http://creativecommons.org/licenses/by-nc/4.0/) by Remko Welling (https://ese.han.nl/~rwelling/) E-mail: remko.welling@han.nl 

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Attribution-NonCommercial 4.0 International License</a>.